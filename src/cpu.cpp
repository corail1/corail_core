// Corail version 1.0, by Benoit Varillon and David Doose
// and Jean-Baptiste Chaudron and Charles Lesir-Cabaniols
// Copyright 2021-2022 ISAE-Supaero, Université de Toulouse, France

// This file is part of the Corail project.

// Corail is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// Corail is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.

// You should have received a copy of the GNU Lesser General Public License
// along with Corail.  If not, see <https://www.gnu.org/licenses/>.
// ---------------------------------------------------------------------------

#include <sched.h>
#include <pthread.h>
#include <iostream>
#include <cstring>

#include "utils/cpu.hpp"

namespace utils
{

    uint64_t total_usage(const struct rusage &usage)
    {
        uint64_t u_sec = usage.ru_utime.tv_sec;
        uint64_t u_micro = usage.ru_utime.tv_usec;
        uint64_t s_sec = usage.ru_stime.tv_sec;
        uint64_t s_micro = usage.ru_stime.tv_usec;

        return (u_sec + s_sec) * 1000000 + (u_micro + s_micro);
    }

    uint64_t burn_cpu(uint64_t microsec)
    {
        struct rusage usage;
        getrusage(RUSAGE_THREAD, &usage);
        uint64_t init = total_usage(usage);
        uint64_t duration = 0;
        while (duration < microsec)
        {
            getrusage(RUSAGE_THREAD, &usage);
            duration = total_usage(usage) - init;
        }
        return duration;
    }
}
