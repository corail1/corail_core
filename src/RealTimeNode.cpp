// Corail version 1.0, by Benoit Varillon and David Doose
// and Jean-Baptiste Chaudron and Charles Lesir-Cabaniols
// Copyright 2021-2022 ISAE-Supaero, Université de Toulouse, France

// This file is part of the Corail project.

// Corail is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// Corail is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.

// You should have received a copy of the GNU Lesser General Public License
// along with Corail.  If not, see <https://www.gnu.org/licenses/>.
// ---------------------------------------------------------------------------

#include "corail_core/RealTimeNode.hpp"

#include "corail_core/RealTimeExecutor.hpp"

namespace corail_core
{

    PeriodicTask::SharedPtr RealTimeNode::create_rt_timer(const std::string &name,
                                                          int cpu, int priority,
                                                          std::chrono::microseconds period,
                                                          std::function<void(void)> callback,
                                                          bool start_on_spin)
    {
        auto task = std::make_shared<PeriodicTask>(name, cpu, priority, period, callback, start_on_spin);
        periodic_tasks_.push_back(task);
        return task;
    }

    ReactiveTask::SharedPtr RealTimeNode::create_reactive_task(const std::string &name,
                                                               uint64_t cpu,
                                                               uint64_t priority,
                                                               std::chrono::microseconds polling_period,
                                                               std::chrono::microseconds running_period,
                                                               std::function<bool(void)> polling_callback,
                                                               std::function<void(void)> execution_callback,
                                                               bool start_on_spin)
    {
        auto task = std::make_shared<ReactiveTask>(name, cpu, priority, running_period, polling_period, polling_callback, execution_callback, start_on_spin);
        reactive_tasks_.push_back(task);
        return task;
    }

    bool RealTimeNode::create_all()
    {
        bool result = true;
        for (auto task : periodic_tasks_)
        {
            if (!task->create())
            {
                result = false;
            }
        }
        for (auto task : subscription_tasks_)
        {
            if (!task->create())
            {
                result = false;
            }
        }
        for (auto task : service_tasks_)
        {
            if (!task->create())
            {
                result = false;
            }
        }
        for (auto task : reactive_tasks_)
        {
            if (!task->create())
            {
                result = false;
            }
        }
        return result;
    }

    bool RealTimeNode::configure_all()
    {
        bool result = true;
        for (auto task : periodic_tasks_)
        {
            if (!task->configure())
            {
                result = false;
            }
        }
        for (auto task : subscription_tasks_)
        {
            if (!task->configure())
            {
                result = false;
            }
        }
        for (auto task : service_tasks_)
        {
            if (!task->configure())
            {
                result = false;
            }
        }
        for (auto task : reactive_tasks_)
        {
            if (!task->configure())
            {
                result = false;
            }
        }
        return result;
    }

    bool RealTimeNode::start_all(std::chrono::nanoseconds release)
    {
        bool result = true;
        for (auto task : periodic_tasks_)
        {
            if (!task->start(release))
            {
                result = false;
            }
        }
        for (auto task : subscription_tasks_)
        {
            if (!task->start(release))
            {
                result = false;
            }
        }
        for (auto task : service_tasks_)
        {
            if (!task->start(release))
            {
                result = false;
            }
        }
        for (auto task : reactive_tasks_)
        {
            if (!task->start(release))
            {
                result = false;
            }
        }
        return result;
    }

    bool RealTimeNode::start_on_spin_all(std::chrono::nanoseconds release)
    {
        bool result = true;
        for (auto task : periodic_tasks_)
        {
            if (task->start_on_spin() && !task->start(release))
            {
                result = false;
            }
        }
        for (auto task : subscription_tasks_)
        {
            if (task->start_on_spin() && !task->start(release))
            {
                result = false;
            }
        }
        for (auto task : service_tasks_)
        {
            if (task->start_on_spin() && !task->start(release))
            {
                result = false;
            }
        }
        for (auto task : reactive_tasks_)
        {
            if (task->start_on_spin() && !task->start(release))
            {
                result = false;
            }
        }
        return result;
    }

    bool RealTimeNode::stop_all()
    {
        bool result = true;
        for (auto task : periodic_tasks_)
        {
            if (!task->stop())
            {
                result = false;
            }
        }
        for (auto task : subscription_tasks_)
        {
            if (!task->stop())
            {
                result = false;
            }
        }
        for (auto task : service_tasks_)
        {
            if (!task->stop())
            {
                result = false;
            }
        }
        for (auto task : reactive_tasks_)
        {
            if (!task->stop())
            {
                result = false;
            }
        }
        return result;
    }

    bool RealTimeNode::cleanup_all()
    {
        bool result = true;
        for (auto task : periodic_tasks_)
        {
            if (!task->cleanup())
            {
                result = false;
            }
        }
        for (auto task : subscription_tasks_)
        {
            if (!task->cleanup())
            {
                result = false;
            }
        }
        for (auto task : service_tasks_)
        {
            if (!task->cleanup())
            {
                result = false;
            }
        }
        for (auto task : reactive_tasks_)
        {
            if (!task->cleanup())
            {
                result = false;
            }
        }
        return result;
    }

    bool RealTimeNode::terminate_all()
    {
        bool result = true;
        for (auto task : periodic_tasks_)
        {
            if (!task->terminate())
            {
                result = false;
            }
        }
        for (auto task : subscription_tasks_)
        {
            if (!task->terminate())
            {
                result = false;
            }
        }
        for (auto task : service_tasks_)
        {
            if (!task->terminate())
            {
                result = false;
            }
        }
        for (auto task : reactive_tasks_)
        {
            if (!task->terminate())
            {
                result = false;
            }
        }
        return result;
    }

    // void RealTimeNode::execute_subscription(rclcpp::SubscriptionBase::SharedPtr sub)
    // {
    //     RealTimeExecutor::execute_subscription(sub);
    // }

}