// Corail version 1.0, by Benoit Varillon and David Doose
// and Jean-Baptiste Chaudron and Charles Lesir-Cabaniols
// Copyright 2021-2022 ISAE-Supaero, Université de Toulouse, France

// This file is part of the Corail project.

// Corail is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// Corail is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.

// You should have received a copy of the GNU Lesser General Public License
// along with Corail.  If not, see <https://www.gnu.org/licenses/>.
// ---------------------------------------------------------------------------

#include "corail_core/RealTimeExecutor.hpp"

#include "utils/rt_tools.hpp"
using namespace utils;

namespace corail_core
{

    void RealTimeExecutor::add_rt_node(RealTimeNode::SharedPtr node)
    {
        rt_nodes_.push_back(node);
    }

    void RealTimeExecutor::add_non_rt_node(rclcpp::Node::SharedPtr node)
    {
        non_rt_nodes_.push_back(node);
    }

    void RealTimeExecutor::spin()
    {
        // Create All
        for (auto node : rt_nodes_)
        {
            node->create_all();
        }
        // Configure All
        for (auto node : rt_nodes_)
        {
            node->configure_all();
        }
        // Start All
        std::chrono::nanoseconds release = std::chrono::nanoseconds(now_ns()) + release_;
        for (auto node : rt_nodes_)
        {
            node->start_on_spin_all(release);
        }

        // Add non real-time nodes
        for (auto node : non_rt_nodes_)
        {
            SingleThreadedExecutor::add_node(node);
        }
        SingleThreadedExecutor::spin();

        // Stop All
        for (auto node : rt_nodes_)
        {
            node->stop_all();
        }
        // Cleanup All
        for (auto node : rt_nodes_)
        {
            node->cleanup_all();
        }
        // Terminate All
        for (auto node : rt_nodes_)
        {
            node->terminate_all();
        }
    }

    void RealTimeExecutor::execute_subscription(rclcpp::SubscriptionBase::SharedPtr sub)
    {
        Executor::execute_subscription(sub);
    }
    void RealTimeExecutor::execute_service(rclcpp::ServiceBase::SharedPtr serv)
    {
        Executor::execute_service(serv);
    }
}