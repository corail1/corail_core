#include "utils/Data.hpp"

namespace utils
{

    template <typename T>
    RtData<T>::RtData(T data) : data_(data), mutex_() {}

    template <typename T>
    void RtData<T>::store(T data)
    {
        mutex_.lock();
        data_ = data;
        mutex_.unlock();
    }

    template <typename T>
    T RtData<T>::load()
    {
        mutex_.lock();
        T data = data_;
        mutex_.unlock();
        return data;
    }
}