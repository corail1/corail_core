// Corail version 1.0, by Benoit Varillon and David Doose
// and Jean-Baptiste Chaudron and Charles Lesir-Cabaniols
// Copyright 2021-2022 ISAE-Supaero, Université de Toulouse, France

// This file is part of the Corail project.

// Corail is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// Corail is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.

// You should have received a copy of the GNU Lesser General Public License
// along with Corail.  If not, see <https://www.gnu.org/licenses/>.
// ---------------------------------------------------------------------------

#include "utils/SynchroThread.hpp"

namespace utils
{

    SynchroThread::SynchroThread(int n)
    {
        pthread_barrierattr_t attr;
        pthread_barrierattr_init(&attr);
        if (pthread_barrier_init(&barrier_, &attr, n) < 0){
            /** @todo error handling */
        }
    }

    SynchroThread::~SynchroThread()
    {
        pthread_barrier_destroy(&barrier_);
    }
} // namspace utils