// Corail version 1.0, by Benoit Varillon and David Doose
// and Jean-Baptiste Chaudron and Charles Lesir-Cabaniols
// Copyright 2021-2022 ISAE-Supaero, Université de Toulouse, France

// This file is part of the Corail project.

// Corail is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// Corail is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.

// You should have received a copy of the GNU Lesser General Public License
// along with Corail.  If not, see <https://www.gnu.org/licenses/>.
// ---------------------------------------------------------------------------

#include "utils/rt_tools.hpp"

#include <string>

#include <pthread.h>
#include <unistd.h>

namespace utils
{
    std::string policy_to_str(const int &policy)
    {
        switch (policy)
        {
        case SCHED_OTHER:
            return "SCHED_OTHER";
        case SCHED_FIFO:
            return "SCHED_FIFO";
        case SCHED_RR:
            return "SCHED_RR";
        default:
            return "UNKNOWN";
        }
    }

    // void add_us(timespec &clock, uint64_t us)
    // {
    //     clock.tv_nsec += (us % 1000000) * 1000;
    //     clock.tv_sec += us / 1000000 + clock.tv_nsec / 1000000000;
    //     clock.tv_nsec %= 1000000000;
    // }

    // void set_us(timespec &clock, uint64_t us)
    // {
    //     clock.tv_nsec = (us % 1000000) * 1000;
    //     clock.tv_sec = us / 1000000 + clock.tv_nsec / 1000000000;
    //     clock.tv_nsec %= 1000000000;
    // }

    void add_ns(timespec &clock, uint64_t ns)
    {
        ns += timespec_to_nsec(clock);
        set_ns(clock, ns);
    }

    void set_ns(timespec &clock, uint64_t ns)
    {
        clock.tv_nsec = ns % 1000000000;
        clock.tv_sec = ns / 1000000000;
    }

    uint64_t now_ns()
    {
        timespec clock;
        clock_gettime(CLOCK, &clock);
        return timespec_to_nsec(clock);
    }

    uint64_t timespec_to_nsec(const timespec &time)
    {
        return static_cast<uint64_t>(time.tv_nsec) + static_cast<uint64_t>(time.tv_sec) * 1000000000;
    }

    void timespec_add(timespec &t1, const timespec &t2)
    {
        t1.tv_nsec += t2.tv_nsec % 1000000000;
        t1.tv_sec += t2.tv_sec + t1.tv_nsec / 1000000000 + t2.tv_nsec / 1000000000;
    }

    bool set_cpu(int cpu)
    {
        cpu_set_t set;
        CPU_ZERO(&set);
        int cpus_nb = sysconf(_SC_NPROCESSORS_ONLN);

        if (cpu >= 0)
        {
            if (cpu >= cpus_nb)
                return false;
            CPU_SET(cpu, &set);
        }
        else
        {
            for (int c = 0; c < cpus_nb; c++)
                CPU_SET(c, &set);
        }

        if (sched_setaffinity(0, sizeof(cpu_set_t), &set) != 0)
            return false;

        return true;
    }

    bool set_priority(int priority)
    {
        sched_param param;
        int policy;

        if (priority > 0)
        {
            param.sched_priority = priority;
            policy = SCHED_FIFO;
        }
        else
        {
            param.sched_priority = 0;
            policy = SCHED_OTHER;
        }
        if (sched_setscheduler(0, policy, &param) != 0)
            return false;

        return true;
    }
}