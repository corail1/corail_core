// Corail version 1.0, by Benoit Varillon and David Doose
// and Jean-Baptiste Chaudron and Charles Lesir-Cabaniols
// Copyright 2021-2022 ISAE-Supaero, Université de Toulouse, France

// This file is part of the Corail project.

// Corail is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// Corail is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.

// You should have received a copy of the GNU Lesser General Public License
// along with Corail.  If not, see <https://www.gnu.org/licenses/>.
// ---------------------------------------------------------------------------

#include "corail_core/PeriodicTask.hpp"
#include "corail_tracing/tracing.hpp"
#include "utils/rt_tools.hpp"

namespace corail_core
{

    std::string state_to_string(PeriodicTaskState status)
    {
        switch (status)
        {
        case PeriodicTaskState::Init:
            return "Init";
        case PeriodicTaskState::Created:
            return "Created";
        case PeriodicTaskState::Configured:
            return "Configured";
        case PeriodicTaskState::Started:
            return "Started";
        case PeriodicTaskState::Running:
            return "Running";
        case PeriodicTaskState::Sleeping:
            return "Sleeping";
        }
        return "Undefined";
    }

    std::ostream &operator<<(std::ostream &out, PeriodicTaskState status)
    {
        out << state_to_string(status);
        return out;
    }

    std::string transition_to_string(PeriodicTaskTransition transition)
    {
        switch (transition)
        {
        case PeriodicTaskTransition::None:
            return "None";
        case PeriodicTaskTransition::Terminate:
            return "Terminate";
        case PeriodicTaskTransition::Configure:
            return "Configure";
        case PeriodicTaskTransition::Cleanup:
            return "Cleanup";
        case PeriodicTaskTransition::Start:
            return "Start";
        case PeriodicTaskTransition::Stop_Sync:
            return "Stop_Sync";
        case PeriodicTaskTransition::Stop_NoSync:
            return "Stop_NoSync";
        }
        return "Undefined";
    }

    std::ostream &operator<<(std::ostream &out, PeriodicTaskTransition transition)
    {
        out << transition_to_string(transition);
        return out;
    }

    bool PeriodicTask::create()
    {
        if (state_ != PeriodicTaskState::Init)
        {
            RCLCPP_ERROR(rclcpp::get_logger(get_name()), "[PeriodicTask::create] wrong state: %s", state_to_string(state_).c_str());
            return false;
        }
        corail::tracing::trigger_transition("create", get_name(), now_ns());
        pthread_barrierattr_t attr;
        pthread_barrierattr_init(&attr);
        if (pthread_barrier_init(&barrier_, &attr, 2) < 0)
        {
            RCLCPP_ERROR(rclcpp::get_logger(get_name()), "[PeriodicTask::create] pthread_barrier_init %d %s", errno, strerror(errno));
            return false;
        }

        pthread_t pthread_id;
        if (pthread_create(&pthread_id, NULL, thread_start, this) < 0)
        {
            RCLCPP_ERROR(rclcpp::get_logger(get_name()), "[PeriodicTask::create] pthread_create %d %s", errno, strerror(errno));
            pthread_barrier_destroy(&barrier_);
            return false;
        }

        state_ = PeriodicTaskState::Created;
        result_ = false;
        pthread_barrier_wait(&barrier_);
        return result_;
    }

    bool PeriodicTask::terminate()
    {
        if (state_ != PeriodicTaskState::Created)
        {
            RCLCPP_ERROR(rclcpp::get_logger(get_name()), "[PeriodicTask::terminate] wrong state: %s", state_to_string(state_).c_str());
            return false;
        }
        corail::tracing::trigger_transition("terminate", get_name(), now_ns());
        transition_ = PeriodicTaskTransition::Terminate;
        result_ = false;
        pthread_barrier_wait(&barrier_);
        pthread_barrier_wait(&barrier_);
        if (result_)
        {
            pthread_barrier_destroy(&barrier_);
            return true;
        }
        else
            return false;
    }

    bool PeriodicTask::configure()
    {
        if (state_ != PeriodicTaskState::Created)
        {
            RCLCPP_ERROR(rclcpp::get_logger(get_name()), "[PeriodicTask::configure] wrong state: %s", state_to_string(state_).c_str());
            return false;
        }
        corail::tracing::trigger_transition("configure", get_name(), now_ns());
        transition_ = PeriodicTaskTransition::Configure;
        result_ = false;
        pthread_barrier_wait(&barrier_);
        // Wait for result
        pthread_barrier_wait(&barrier_);
        return result_;
    }

    bool PeriodicTask::cleanup()
    {
        if (state_ != PeriodicTaskState::Configured)
        {
            RCLCPP_ERROR(rclcpp::get_logger(get_name()), "[PeriodicTask::cleanup] wrong state: %s", state_to_string(state_).c_str());
            return false;
        }
        corail::tracing::trigger_transition("cleanup", get_name(), now_ns());
        transition_ = PeriodicTaskTransition::Cleanup;
        result_ = false;
        pthread_barrier_wait(&barrier_);
        pthread_barrier_wait(&barrier_);
        return result_;
    }

    bool PeriodicTask::start(std::chrono::nanoseconds release)
    {
        if (state_ != PeriodicTaskState::Configured)
        {
            RCLCPP_ERROR(rclcpp::get_logger(get_name()), "[PeriodicTask::start] wrong state: %s", state_to_string(state_).c_str());
            return false;
        }
        auto now = now_ns();
        if (release.count() <= 0)
        {
            release = std::chrono::nanoseconds(now);
        }
        corail::tracing::trigger_transition("start", get_name(), now);
        set_ns(next_release_, release.count());
        transition_ = PeriodicTaskTransition::Start;
        result_ = false;
        pthread_barrier_wait(&barrier_);
        pthread_barrier_wait(&barrier_);
        return result_;
    }

    bool PeriodicTask::stop(bool synchronized)
    {
        if (state_ == PeriodicTaskState::Configured)
        {
            return true;
        }
        if (state_ != PeriodicTaskState::Started &&
            state_ != PeriodicTaskState::Running &&
            state_ != PeriodicTaskState::Sleeping)
        {
            RCLCPP_ERROR(rclcpp::get_logger(get_name()), "[PeriodicTask::stop] wrong state: %s", state_to_string(state_).c_str());
            return false;
        }

        corail::tracing::trigger_transition("stop", get_name(), now_ns());
        pthread_t current_id = pthread_self();
        if (synchronized && !pthread_equal(self_id_, current_id))
        {
            transition_ = PeriodicTaskTransition::Stop_Sync;
            result_ = false;
            pthread_barrier_wait(&barrier_);
            return result_;
        }
        else
        {
            transition_ = PeriodicTaskTransition::Stop_NoSync;
            return true;
        }
    }

    void *PeriodicTask::thread_start(void *data)
    {
        auto self = (PeriodicTask *)data;
        self->self_id_ = pthread_self();
        self->step();
        return NULL;
    }

    void PeriodicTask::step()
    {
        result_ = true;
        pthread_barrier_wait(&barrier_);

    loop:
        corail::tracing::state_begin(state_to_string(state_), get_name(), timespec_to_nsec(next_release_), now_ns());
        switch (state_)
        {
        //     //------------------------- Init -------------------------
        case PeriodicTaskState::Init:
            pthread_barrier_wait(&barrier_);
            transition_ = PeriodicTaskTransition::None;
            goto loop;
        //------------------------- Created -------------------------
        case PeriodicTaskState::Created:
        {
            pthread_barrier_wait(&barrier_);
            switch (transition_)
            {
            //----------> Terminate <----------
            case PeriodicTaskTransition::Terminate:
            {
                state_ = PeriodicTaskState::Init;
                transition_ = PeriodicTaskTransition::None;
                result_ = true;
                pthread_barrier_wait(&barrier_);
                return;
            }
            //----------> Configure <----------
            case PeriodicTaskTransition::Configure:
            {
                mutex_.lock();
                if (state_ != PeriodicTaskState::Created)
                {
                    RCLCPP_ERROR(rclcpp::get_logger(get_name()), "[step::configure] wrong status");
                    mutex_.unlock();
                    transition_ = PeriodicTaskTransition::None;
                    goto loop;
                }

                if (!set_cpu(cpu_))
                {
                    RCLCPP_ERROR(rclcpp::get_logger(get_name()), "[step::configure] set_cpu %d %s", errno, strerror(errno));
                    mutex_.unlock();
                    transition_ = PeriodicTaskTransition::None;
                    goto loop;
                }

                if (!set_priority(priority_))
                {
                    RCLCPP_ERROR(rclcpp::get_logger(get_name()), "[step::configure] set_priority %d %s", errno, strerror(errno));
                    mutex_.unlock();
                    transition_ = PeriodicTaskTransition::None;
                    goto loop;
                }
                mutex_.unlock();
                state_ = PeriodicTaskState::Configured;
                transition_ = PeriodicTaskTransition::None;
                result_ = true;
                pthread_barrier_wait(&barrier_);
                goto loop;
            }
            //----------> ? <----------
            default:
                RCLCPP_ERROR(rclcpp::get_logger(get_name()),
                             "[step] undefined transition %s -> %s",
                             state_to_string(state_).c_str(),
                             transition_to_string(transition_).c_str());
                transition_ = PeriodicTaskTransition::None;
                goto loop;
            }
        }
            //------------------------- Configured -------------------------
        case PeriodicTaskState::Configured:
        {
            pthread_barrier_wait(&barrier_);
            switch (transition_)
            {
            //----------> Cleanup <----------
            case PeriodicTaskTransition::Cleanup:
            {
                state_ = PeriodicTaskState::Created;
                transition_ = PeriodicTaskTransition::None;
                result_ = true;
                pthread_barrier_wait(&barrier_);
                goto loop;
            }
            //----------> Start <----------
            case PeriodicTaskTransition::Start:
            {
                state_ = PeriodicTaskState::Started;
                transition_ = PeriodicTaskTransition::None;
                result_ = true;
                pthread_barrier_wait(&barrier_);
                goto loop;
            }
            //----------> ? <----------
            default:
                RCLCPP_ERROR(rclcpp::get_logger(get_name()),
                             "[step] undefined transition %s -> %s",
                             state_to_string(state_).c_str(),
                             transition_to_string(transition_).c_str());
                transition_ = PeriodicTaskTransition::None;
                goto loop;
            }
        }
            //------------------------- Started -------------------------
        case PeriodicTaskState::Started:
        {
            clock_nanosleep(CLOCK, TIMER_ABSTIME, &next_release_, 0);
            switch (transition_)
            {
            //----------> Stop Sync <----------
            case PeriodicTaskTransition::Stop_Sync:
            {
                state_ = PeriodicTaskState::Configured;
                transition_ = PeriodicTaskTransition::None;
                result_ = true;
                pthread_barrier_wait(&barrier_);
                goto loop;
            }
            //----------> Stop NoSync <----------
            case PeriodicTaskTransition::Stop_NoSync:
            {
                state_ = PeriodicTaskState::Configured;
                transition_ = PeriodicTaskTransition::None;
                result_ = true;
                goto loop;
            }
            //----------> Running <----------
            default:
                state_ = PeriodicTaskState::Running;
                goto loop;
            }
        }
            //------------------------- Running -------------------------
        case PeriodicTaskState::Running:
        {
            callback_();
            switch (transition_)
            {
            //----------> Stop Sync <----------
            case PeriodicTaskTransition::Stop_Sync:
            {
                state_ = PeriodicTaskState::Configured;
                transition_ = PeriodicTaskTransition::None;
                result_ = true;
                pthread_barrier_wait(&barrier_);
                goto loop;
            }
            //----------> Stop NoSync <----------
            case PeriodicTaskTransition::Stop_NoSync:
            {
                state_ = PeriodicTaskState::Configured;
                transition_ = PeriodicTaskTransition::None;
                result_ = true;
                goto loop;
            }
            //----------> Sleeping <----------
            default:
                state_ = PeriodicTaskState::Sleeping;
                goto loop;
            }
        }
            //------------------------- Sleeping -------------------------
        case PeriodicTaskState::Sleeping:
        {
            add_ns(next_release_, period_.load().count());
            auto now = now_ns();
            while (now > timespec_to_nsec(next_release_))
            {
                add_ns(next_release_, period_.load().count());
            }
            clock_nanosleep(CLOCK, TIMER_ABSTIME, &next_release_, 0);
            switch (transition_)
            {
            //----------> Stop Sync <----------
            case PeriodicTaskTransition::Stop_Sync:
            {
                state_ = PeriodicTaskState::Configured;
                transition_ = PeriodicTaskTransition::None;
                result_ = true;
                pthread_barrier_wait(&barrier_);
                goto loop;
            }
            //----------> Stop NoSync <----------
            case PeriodicTaskTransition::Stop_NoSync:
            {
                state_ = PeriodicTaskState::Configured;
                transition_ = PeriodicTaskTransition::None;
                result_ = true;
                goto loop;
            }
            //----------> Running <----------
            default:
                state_ = PeriodicTaskState::Running;
                goto loop;
            }
        }
        }
    }
} // namespace corail_core
