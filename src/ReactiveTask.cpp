// Corail version 1.0, by Benoit Varillon and David Doose
// and Jean-Baptiste Chaudron and Charles Lesir-Cabaniols
// Copyright 2021-2022 ISAE-Supaero, Université de Toulouse, France

// This file is part of the Corail project.

// Corail is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// Corail is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.

// You should have received a copy of the GNU Lesser General Public License
// along with Corail.  If not, see <https://www.gnu.org/licenses/>.
// ---------------------------------------------------------------------------

#include "corail_core/ReactiveTask.hpp"
#include "corail_tracing/tracing.hpp"
#include "utils/rt_tools.hpp"

using namespace std::chrono_literals;

namespace corail_core
{

    std::string state_to_string(ReactiveTaskState status)
    {
        switch (status)
        {
        case ReactiveTaskState::Init:
            return "Init";
        case ReactiveTaskState::Created:
            return "Created";
        case ReactiveTaskState::Configured:
            return "Configured";
        case ReactiveTaskState::Started:
            return "Started";
        case ReactiveTaskState::Polling:
            return "Polling";
        case ReactiveTaskState::Running:
            return "Running";
        case ReactiveTaskState::SleepingJitter:
            return "SleepingJitter";
        case ReactiveTaskState::SleepingPeriod:
            return "SleepingPeriod";
        }
        return "Undefined";
    }

    std::ostream &operator<<(std::ostream &out, ReactiveTaskState status)
    {
        out << state_to_string(status);
        return out;
    }

    std::string transition_to_string(ReactiveTaskTransition transition)
    {
        switch (transition)
        {
        case ReactiveTaskTransition::None:
            return "None";
        case ReactiveTaskTransition::Terminate:
            return "Terminate";
        case ReactiveTaskTransition::Configure:
            return "Configure";
        case ReactiveTaskTransition::Cleanup:
            return "Cleanup";
        case ReactiveTaskTransition::Start:
            return "Start";
        case ReactiveTaskTransition::Stop_Sync:
            return "Stop_Sync";
        case ReactiveTaskTransition::Stop_NoSync:
            return "Stop_NoSync";
        }
        return "Undefined";
    }

    std::ostream &operator<<(std::ostream &out, ReactiveTaskTransition transition)
    {
        out << transition_to_string(transition);
        return out;
    }

    bool ReactiveTask::create()
    {
        if (state_ != ReactiveTaskState::Init)
        {
            RCLCPP_ERROR(rclcpp::get_logger(get_name()), "[ReactiveTask::create] wrong state: %s", state_to_string(state_).c_str());
            return false;
        }
        corail::tracing::trigger_transition("create", get_name(), now_ns());
        pthread_barrierattr_t attr;
        pthread_barrierattr_init(&attr);
        if (pthread_barrier_init(&barrier_, &attr, 2) < 0)
        {
            RCLCPP_ERROR(rclcpp::get_logger(get_name()), "[ReactiveTask::create] pthread_barrier_init %d %s", errno, strerror(errno));
            return false;
        }

        pthread_t pthread_id;
        if (pthread_create(&pthread_id, NULL, thread_start, this) < 0)
        {
            RCLCPP_ERROR(rclcpp::get_logger(get_name()), "[ReactiveTask::create] pthread_create %d %s", errno, strerror(errno));
            pthread_barrier_destroy(&barrier_);
            return false;
        }

        state_ = ReactiveTaskState::Created;
        result_ = false;
        pthread_barrier_wait(&barrier_);
        return result_;
    }

    bool ReactiveTask::terminate()
    {
        if (state_ != ReactiveTaskState::Created)
        {
            RCLCPP_ERROR(rclcpp::get_logger(get_name()), "[ReactiveTask::terminate] wrong state: %s", state_to_string(state_).c_str());
            return false;
        }
        corail::tracing::trigger_transition("terminate", get_name(), now_ns());
        transition_ = ReactiveTaskTransition::Terminate;
        result_ = false;
        pthread_barrier_wait(&barrier_);
        pthread_barrier_wait(&barrier_);
        if (result_)
        {
            pthread_barrier_destroy(&barrier_);
            return true;
        }
        else
            return false;
    }

    bool ReactiveTask::configure()
    {
        if (state_ != ReactiveTaskState::Created)
        {
            RCLCPP_ERROR(rclcpp::get_logger(get_name()), "[ReactiveTask::configure] wrong state: %s", state_to_string(state_).c_str());
            return false;
        }
        corail::tracing::trigger_transition("configure", get_name(), now_ns());
        transition_ = ReactiveTaskTransition::Configure;
        result_ = false;
        pthread_barrier_wait(&barrier_);
        // Wait for result
        pthread_barrier_wait(&barrier_);
        return result_;
    }

    bool ReactiveTask::cleanup()
    {
        if (state_ != ReactiveTaskState::Configured)
        {
            RCLCPP_ERROR(rclcpp::get_logger(get_name()), "[ReactiveTask::cleanup] wrong state: %s", state_to_string(state_).c_str());
            return false;
        }
        corail::tracing::trigger_transition("cleanup", get_name(), now_ns());
        transition_ = ReactiveTaskTransition::Cleanup;
        result_ = false;
        pthread_barrier_wait(&barrier_);
        pthread_barrier_wait(&barrier_);
        return result_;
    }

    bool ReactiveTask::start(std::chrono::nanoseconds release)
    {
        if (state_ != ReactiveTaskState::Configured)
        {
            RCLCPP_ERROR(rclcpp::get_logger(get_name()), "[ReactiveTask::start] wrong state: %s", state_to_string(state_).c_str());
            return false;
        }
        auto now = now_ns();
        if (release.count() <= 0)
        {
            release = std::chrono::nanoseconds(now);
        }
        corail::tracing::trigger_transition("start", get_name(), now);
        set_ns(next_release_, release.count());
        transition_ = ReactiveTaskTransition::Start;
        result_ = false;
        pthread_barrier_wait(&barrier_);
        pthread_barrier_wait(&barrier_);
        return result_;
    }

    bool ReactiveTask::stop(bool synchronized)
    {
        if (state_ == ReactiveTaskState::Configured)
        {
            return true;
        }
        if (state_ != ReactiveTaskState::Started &&
            state_ != ReactiveTaskState::Running &&
            state_ != ReactiveTaskState::Polling &&
            state_ != ReactiveTaskState::SleepingJitter &&
            state_ != ReactiveTaskState::SleepingPeriod)
        {
            RCLCPP_ERROR(rclcpp::get_logger(get_name()), "[ReactiveTask::stop] wrong state: %s", state_to_string(state_).c_str());
            return false;
        }

        corail::tracing::trigger_transition("stop", get_name(), now_ns());
        pthread_t current_id = pthread_self();
        if (synchronized && !pthread_equal(self_id_, current_id))
        {
            transition_ = ReactiveTaskTransition::Stop_Sync;
            result_ = false;
            pthread_barrier_wait(&barrier_);
            return result_;
        }
        else
        {
            transition_ = ReactiveTaskTransition::Stop_NoSync;
            return true;
        }
    }

    void *ReactiveTask::thread_start(void *data)
    {
        auto self = (ReactiveTask *)data;
        self->self_id_ = pthread_self();
        self->step();
        return NULL;
    }

    void ReactiveTask::step()
    {
        result_ = true;
        pthread_barrier_wait(&barrier_);

    loop:
        corail::tracing::state_begin(state_to_string(state_), get_name(), timespec_to_nsec(next_release_), now_ns());
        switch (state_)
        {
        //     //------------------------- Init -------------------------
        case ReactiveTaskState::Init:
            pthread_barrier_wait(&barrier_);
            transition_ = ReactiveTaskTransition::None;
            goto loop;
        //------------------------- Created -------------------------
        case ReactiveTaskState::Created:
        {
            pthread_barrier_wait(&barrier_);
            switch (transition_)
            {
            //----------> Terminate <----------
            case ReactiveTaskTransition::Terminate:
            {
                state_ = ReactiveTaskState::Init;
                transition_ = ReactiveTaskTransition::None;
                result_ = true;
                pthread_barrier_wait(&barrier_);
                return;
            }
            //----------> Configure <----------
            case ReactiveTaskTransition::Configure:
            {
                mutex_.lock();
                if (state_ != ReactiveTaskState::Created)
                {
                    RCLCPP_ERROR(rclcpp::get_logger(get_name()), "[step::configure] wrong status");
                    mutex_.unlock();
                    transition_ = ReactiveTaskTransition::None;
                    goto loop;
                }

                if (!set_cpu(cpu_))
                {
                    RCLCPP_ERROR(rclcpp::get_logger(get_name()), "[step::configure] set_cpu %d %s", errno, strerror(errno));
                    mutex_.unlock();
                    transition_ = ReactiveTaskTransition::None;
                    goto loop;
                }

                if (!set_priority(priority_))
                {
                    RCLCPP_ERROR(rclcpp::get_logger(get_name()), "[step::configure] set_priority %d %s", errno, strerror(errno));
                    mutex_.unlock();
                    transition_ = ReactiveTaskTransition::None;
                    goto loop;
                }
                mutex_.unlock();
                state_ = ReactiveTaskState::Configured;
                transition_ = ReactiveTaskTransition::None;
                result_ = true;
                pthread_barrier_wait(&barrier_);
                goto loop;
            }
            //----------> ? <----------
            default:
                RCLCPP_ERROR(rclcpp::get_logger(get_name()),
                             "[step] undefined transition %s -> %s",
                             state_to_string(state_).c_str(),
                             transition_to_string(transition_).c_str());
                transition_ = ReactiveTaskTransition::None;
                goto loop;
            }
        }
            //------------------------- Configured -------------------------
        case ReactiveTaskState::Configured:
        {
            pthread_barrier_wait(&barrier_);
            switch (transition_)
            {
            //----------> Cleanup <----------
            case ReactiveTaskTransition::Cleanup:
            {
                state_ = ReactiveTaskState::Created;
                transition_ = ReactiveTaskTransition::None;
                result_ = true;
                pthread_barrier_wait(&barrier_);
                goto loop;
            }
            //----------> Start <----------
            case ReactiveTaskTransition::Start:
            {
                state_ = ReactiveTaskState::Started;
                transition_ = ReactiveTaskTransition::None;
                result_ = true;
                pthread_barrier_wait(&barrier_);
                goto loop;
            }
            //----------> ? <----------
            default:
                RCLCPP_ERROR(rclcpp::get_logger(get_name()),
                             "[step] undefined transition %s -> %s",
                             state_to_string(state_).c_str(),
                             transition_to_string(transition_).c_str());
                transition_ = ReactiveTaskTransition::None;
                goto loop;
            }
        }
            //------------------------- Started -------------------------
        case ReactiveTaskState::Started:
        {
            clock_nanosleep(CLOCK, TIMER_ABSTIME, &next_release_, 0);
            switch (transition_)
            {
            //----------> Stop Sync <----------
            case ReactiveTaskTransition::Stop_Sync:
            {
                state_ = ReactiveTaskState::Configured;
                transition_ = ReactiveTaskTransition::None;
                result_ = true;
                pthread_barrier_wait(&barrier_);
                goto loop;
            }
            //----------> Running <----------
            default:
                state_ = ReactiveTaskState::Polling;
                goto loop;
            }
        }
            //------------------------- Polling -------------------------
        case ReactiveTaskState::Polling:
        {
            auto result = polling_callback_();
            switch (transition_)
            {
            //----------> Stop Sync <----------
            case ReactiveTaskTransition::Stop_Sync:
            {
                state_ = ReactiveTaskState::Configured;
                transition_ = ReactiveTaskTransition::None;
                result_ = true;
                pthread_barrier_wait(&barrier_);
                goto loop;
            }
            //----------> Stop NoSync <----------
            case ReactiveTaskTransition::Stop_NoSync:
            {
                state_ = ReactiveTaskState::Configured;
                transition_ = ReactiveTaskTransition::None;
                result_ = true;
                goto loop;
            }
            default:
            {
                if (result)
                {
                    state_ = ReactiveTaskState::Running;
                    goto loop;
                }
                else
                {
                    state_ = ReactiveTaskState::SleepingJitter;
                    goto loop;
                }
            }
            }
        }
            //------------------------- Running -------------------------
        case ReactiveTaskState::Running:
        {

            execution_callback_();

            switch (transition_)
            {
            //----------> Stop Sync <----------
            case ReactiveTaskTransition::Stop_Sync:
            {
                state_ = ReactiveTaskState::Configured;
                transition_ = ReactiveTaskTransition::None;
                result_ = true;
                pthread_barrier_wait(&barrier_);
                goto loop;
            }
            //----------> Sleeping <----------
            default:
                state_ = ReactiveTaskState::SleepingPeriod;
                goto loop;
            }
        }
            //------------------------- Sleeping -------------------------
        case ReactiveTaskState::SleepingPeriod:
        {
            add_ns(next_release_, period_.load().count());
            auto now = now_ns();
            while (now > timespec_to_nsec(next_release_))
            {
                add_ns(next_release_, period_.load().count());
            }
            clock_nanosleep(CLOCK, TIMER_ABSTIME, &next_release_, 0);
            switch (transition_)
            {
            //----------> Stop Sync <----------
            case ReactiveTaskTransition::Stop_Sync:
            {
                state_ = ReactiveTaskState::Configured;
                transition_ = ReactiveTaskTransition::None;
                result_ = true;
                pthread_barrier_wait(&barrier_);
                goto loop;
            }
            //----------> Stop NoSync <----------
            case ReactiveTaskTransition::Stop_NoSync:
            {
                state_ = ReactiveTaskState::Configured;
                transition_ = ReactiveTaskTransition::None;
                result_ = true;
                goto loop;
            }
            //----------> Running <----------
            default:
                state_ = ReactiveTaskState::Polling;
                goto loop;
            }
        }
            //------------------------- Sleeping -------------------------
        case ReactiveTaskState::SleepingJitter:
        {
            add_ns(next_release_, jitter_.load().count());
            auto now = now_ns();
            while (now > timespec_to_nsec(next_release_))
            {
                add_ns(next_release_, jitter_.load().count());
            }
            clock_nanosleep(CLOCK, TIMER_ABSTIME, &next_release_, 0);
            switch (transition_)
            {
            //----------> Stop Sync <----------
            case ReactiveTaskTransition::Stop_Sync:
            {
                state_ = ReactiveTaskState::Configured;
                transition_ = ReactiveTaskTransition::None;
                result_ = true;
                pthread_barrier_wait(&barrier_);
                goto loop;
            }
            //----------> Stop NoSync <----------
            case ReactiveTaskTransition::Stop_NoSync:
            {
                state_ = ReactiveTaskState::Configured;
                transition_ = ReactiveTaskTransition::None;
                result_ = true;
                goto loop;
            }
            //----------> Polling <----------
            default:
                state_ = ReactiveTaskState::Polling;
                goto loop;
            }
        }
        }
    }
} // namespave corail_core