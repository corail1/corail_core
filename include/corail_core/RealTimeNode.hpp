// Corail version 1.0, by Benoit Varillon and David Doose
// and Jean-Baptiste Chaudron and Charles Lesir-Cabaniols
// Copyright 2021-2022 ISAE-Supaero, Université de Toulouse, France

// This file is part of the Corail project.

// Corail is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// Corail is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.

// You should have received a copy of the GNU Lesser General Public License
// along with Corail.  If not, see <https://www.gnu.org/licenses/>.
// ---------------------------------------------------------------------------

#ifndef CORAIL_CORE__REALTIME_NODE_HPP
#define CORAIL_CORE__REALTIME_NODE_HPP

#include <string>
#include <functional>
#include <chrono>

#include "corail_core/PeriodicTask.hpp"
#include "corail_core/TaskBase.hpp"
#include "corail_core/SubscriptionTask.hpp"
#include "corail_core/ServiceTask.hpp"
#include "corail_core/ReactiveTask.hpp"

#include "rclcpp/rclcpp.hpp"

namespace corail_core
{

    /**
     * @brief Base class for creating corail realtime node.
     *
     */
    class RealTimeNode : public rclcpp::Node
    {
    public:
        RCLCPP_SMART_PTR_DEFINITIONS(RealTimeNode)

        /**
         * @brief Construct a new Realtime Node. \sa rclcpp::Node
         *
         * @param[in] name Name of the node.
         * @param[in] ns Namespace of the node.
         * @param[in] option Additional optionns to control creation of the node. ( \sa rclcpp::NodeOptions ).
         * @throws InvalidNamespaceError if the namespace is invalid.
         */
        RealTimeNode(const std::string &name, const std::string &ns,
                     const rclcpp::NodeOptions option = rclcpp::NodeOptions())
            : Node(name, ns, option) {}

        /**
         * @brief Construct a new Realtime Node. \sa rclcpp::Node
         *
         * @param[in] name Name of the node.
         * @param[in] option Additional optionns to control creation of the node. ( \sa rclcpp::NodeOptions ).
         * @throws InvalidNamespaceError if the namespace is invalid.
         */
        RealTimeNode(const std::string &name,
                     const rclcpp::NodeOptions option = rclcpp::NodeOptions())
            : Node(name, option) {}

        /**
         * @brief Create a realtime Timer.
         *
         * @param name Name of the task.
         * @param cpu The cpu on which the timer will run. If <0 the subscriber can use all the cpu.
         * @param priority The (POSIX) priority of the subscriber, if >0 -> SHED_FIFO, if =0 -> SHED_OTHER.
         * @param period The duration of each period.
         * @param callback The function to execute at each period.
         * @return PeriodicTask::SharedPtr A shared pointer to the created timer.
         */
        PeriodicTask::SharedPtr create_rt_timer(const std::string &name,
                                                int cpu,
                                                int priority,
                                                std::chrono::microseconds period,
                                                std::function<void(void)> callback,
                                                bool start_on_spin = true);

        /// Create and return a ReactiveTask.
        /**
         * \param[in] name Name of the task.
         * \param[in] cpu The cpu on which the task will run. If <0 the subscriber can use all the cpu.
         * \param[in] priority The (POSIX) priority of the subscriber, if >0 -> SHED_FIFO, if =0 -> SHED_OTHER.
         * \param[in] polling_period The time to wait between two polling.
         * \param[in] running_period The time to wait before polling after the execution of the execution_callbak.
         * \param[in] polling_callback The function to execute et each periode.
         * \param[in] execution_callback The function to execute if the polling callback return true.
         * \return Shared pointer to the created rective task.
         */
        ReactiveTask::SharedPtr create_reactive_task(const std::string &name,
                                                     uint64_t cpu,
                                                     uint64_t priority,
                                                     std::chrono::microseconds polling_period,
                                                     std::chrono::microseconds running_period,
                                                     std::function<bool(void)> polling_callback,
                                                     std::function<void(void)> execution_callback,
                                                     bool start_on_spin = true);

        /// Create and return a Subscription.
        /**
         * \param[in] name Name of the task.
         * \param[in] cpu The cpu on which the subscription will run. If <0 the subscriber can use all the cpu.
         * \param[in] priority The (POSIX) priority of the subscriber, if >0 -> SHED_FIFO, if =0 -> SHED_OTHER.
         * \param[in] polling_period The time to wait between two polling.
         * \param[in] running_period The time to wait before polling after the execution of the callbak.
         * \param[in] topic_name The topic to subscribe on.
         * \param[in] qos QoS profile for Subcription.
         * \param[in] callback The user-defined callback function to receive a message
         * \param[in] options Additional options for the creation of the Subscription.
         * \param[in] msg_mem_strat The message memory strategy to use for allocating messages.
         * \return Shared pointer to the created subscription.
         */
        template <
            typename MessageT,
            typename CallbackT,
            typename AllocatorT = std::allocator<void>,
            typename CallbackMessageT =
                typename rclcpp::subscription_traits::has_message_type<CallbackT>::type,
            typename SubscriptionT = rclcpp::Subscription<CallbackMessageT, AllocatorT>,
            typename MessageMemoryStrategyT = rclcpp::message_memory_strategy::MessageMemoryStrategy<
                CallbackMessageT,
                AllocatorT>>
        typename SubscriptionTask<MessageT>::SharedPtr
        // SubscriptionTask::SharedPtr
        create_rt_subscription(
            const std::string &name,
            uint64_t cpu,
            uint64_t priority,
            std::chrono::microseconds polling_period,
            std::chrono::microseconds running_period,
            const std::string &topic_name,
            const rclcpp::QoS &qos,
            CallbackT &&callback,
            bool start_on_spin = true,
            const rclcpp::SubscriptionOptionsWithAllocator<AllocatorT> &options =
                rclcpp::SubscriptionOptionsWithAllocator<AllocatorT>(),
            typename MessageMemoryStrategyT::SharedPtr msg_mem_strat = (MessageMemoryStrategyT::create_default()));

        /// Create and return a Service.
        /**
         * \param[in] name Name of the task.
         * \param[in] cpu The cpu on which the subscription will run. If <0 the subscriber can use all the cpu.
         * \param[in] priority The (POSIX) priority of the subscriber, if >0 -> SHED_FIFO, if =0 -> SHED_OTHER.
         * \param[in] polling_period The time to wait between two polling.
         * \param[in] running_period The time to wait before polling after the execution of the callbak.
         * \param[in] service_name The topic to service on.
         * \param[in] callback User-defined callback function.
         * \param[in] qos_profile rmw_qos_profile_t Quality of service profile for client.
         * \param[in] group Callback group to call the service.
         * \return Shared pointer to the created service.
         */
        template <
            typename ServiceT,
            typename CallbackT>
        typename ServiceTask<ServiceT>::SharedPtr
        create_rt_service(
            const std::string name,
            uint64_t cpu,
            uint64_t priority,
            std::chrono::microseconds polling_period,
            std::chrono::microseconds running_period,
            const std::string &service_name,
            CallbackT &&callback,
            bool start_on_spin = true,
            const rmw_qos_profile_t &qos_profile = rmw_qos_profile_services_default,
            rclcpp::CallbackGroup::SharedPtr group = nullptr);

        /**
         * @brief Call the create transisiton for all the tasks of the node.
         *
         * @return true
         * @return false
         */
        bool create_all();

        /**
         * @brief Call the configure transisiton for all the tasks of the node.
         *
         * @return true
         * @return false
         */
        bool configure_all();

        /**
         * @brief Call the start transisiton for all the tasks of the node.
         *
         * @param release The realease date of all the tasks.
         * @return true
         * @return false
         */
        bool start_all(std::chrono::nanoseconds release);

        bool start_on_spin_all(std::chrono::nanoseconds release);

        /**
         * @brief Call the stop transisiton for all the tasks of the node.
         *
         * @return true
         * @return false
         */
        bool stop_all();

        /**
         * @brief Call the cleanup transisiton for all the tasks of the node.
         *
         * @return true
         * @return false
         */
        bool cleanup_all();

        /**
         * @brief Call the terminate transisiton for all the tasks of the node.
         *
         * @return true
         * @return false
         */
        bool terminate_all();

    private:
        std::vector<TaskBase::SharedPtr> periodic_tasks_;
        std::vector<TaskBase::SharedPtr> subscription_tasks_;
        std::vector<TaskBase::SharedPtr> service_tasks_;
        std::vector<TaskBase::SharedPtr> reactive_tasks_;
    };
}

#include "RealTimeNode_impl.hpp"

#endif