// Corail version 1.0, by Benoit Varillon and David Doose
// and Jean-Baptiste Chaudron and Charles Lesir-Cabaniols
// Copyright 2021-2022 ISAE-Supaero, Université de Toulouse, France

// This file is part of the Corail project.

// Corail is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// Corail is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.

// You should have received a copy of the GNU Lesser General Public License
// along with Corail.  If not, see <https://www.gnu.org/licenses/>.
// ---------------------------------------------------------------------------

#ifndef CORAIL_CORE__TASK_BASE_HPP
#define CORAIL_CORE__TASK_BASE_HPP

// #include"corail_core/RealTimeExecutor.hpp"
// #include "corail_core/RealTimeNode.hpp"

#include <chrono>
#include "rclcpp/rclcpp.hpp"

namespace corail_core
{
    /**
     * @brief Interface used to manipulate tasks
     *
     */
    class TaskBase
    {
    public:
        RCLCPP_SHARED_PTR_DEFINITIONS(TaskBase)
        RCLCPP_WEAK_PTR_DEFINITIONS(TaskBase)

        virtual ~TaskBase(){};

        /**
         * @brief Trigger the create transition.
         *
         * Launch the creation of the thread in which the task will execute.
         * Only allowed in init state. -> created
         *
         * @return true If the transition succed
         * @return false Otherwise
         */
        virtual bool create() = 0;

        /**
         * @brief Trigger the configure transition.
         *
         * Configure the thread priority and cpu.
         * Only allowed in created state. -> configured
         *
         * @return true If the transition succed
         * @return false Otherwise
         */
        virtual bool configure() = 0;

        /**
         *
         * @brief Trigger the cleanup transition.
         *
         * Only allowed in configured state. -> created
         * @return true If the transition succed
         * @return false Otherwise
         */
        virtual bool cleanup() = 0;

        /**
         * @brief Trigger the start transition.
         *
         * Lauch execution of the task at the asked date.
         * Only allowed in configured state. -> started
         *
         * @param[in] release The date of the first execution of the callback.
         * @return true If the transition succed
         * @return false Otherwise
         */
        virtual bool start(std::chrono::nanoseconds release = std::chrono::nanoseconds(0)) = 0;

        /**
         * @brief Trigger the stop transition.
         *
         * Stop the execution of the task.
         * Only allowed in started, running or sleeping states. -> configured
         *
         * @return true If the transition succed
         * @return false Otherwise
         */
        virtual bool stop(bool synchronized = true) = 0;

        /**
         * @brief Trigger the terminate transition.
         *
         * Will destruct the thread of the task.
         * Only allowed in created state. -> init
         *
         * @return true If the transition succed
         * @return false Otherwises
         */
        virtual bool terminate() = 0;

        virtual bool start_on_spin() const = 0;

    protected:
        static void execute_subscription(rclcpp::SubscriptionBase::SharedPtr sub);
        static void execute_service(rclcpp::ServiceBase::SharedPtr sub);
    };

} // namespace corail_core
#endif