// Corail version 1.0, by Benoit Varillon and David Doose
// and Jean-Baptiste Chaudron and Charles Lesir-Cabaniols
// Copyright 2021-2022 ISAE-Supaero, Université de Toulouse, France

// This file is part of the Corail project.

// Corail is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// Corail is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.

// You should have received a copy of the GNU Lesser General Public License
// along with Corail.  If not, see <https://www.gnu.org/licenses/>.
// ---------------------------------------------------------------------------

#ifndef CORAIL_CORE__CORRAIL_CORE_HPP
#define CORAIL_CORE__CORRAIL_CORE_HPP

#include "corail_core/RealTimeExecutor.hpp"
#include "corail_core/SubscriptionTask.hpp"
#include "corail_core/PeriodicTask.hpp"
#include "corail_core/RealTimeNode.hpp"
#include "corail_core/ServiceTask.hpp"
#include "corail_core/ReactiveTask.hpp"

#endif