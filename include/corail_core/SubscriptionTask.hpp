// Corail version 1.0, by Benoit Varillon and David Doose
// and Jean-Baptiste Chaudron and Charles Lesir-Cabaniols
// Copyright 2021-2022 ISAE-Supaero, Université de Toulouse, France

// This file is part of the Corail project.

// Corail is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// Corail is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.

// You should have received a copy of the GNU Lesser General Public License
// along with Corail.  If not, see <https://www.gnu.org/licenses/>.
// ---------------------------------------------------------------------------

#ifndef CORAIL_CORE__SUBSCRIPTION_TASK_HPP
#define CORAIL_CORE__SUBSCRIPTION_TASK_HPP

#include <pthread.h>
#include <atomic>
#include <string>
#include <functional>
#include <chrono>
using namespace std::chrono_literals;

#include "utils/RtMutex.hpp"
// #include "corail_core/TaskBase.hpp"
#include "corail_core/ReactiveTask.hpp"
using namespace utils;

#include "rclcpp/rclcpp.hpp"

namespace corail_core
{
    /**
     * @brief Use ReactiveTask to implement a realtime ROS 2 subscription.
     *
     * See \sa(ReactiveTask).
     *
     */
    template <typename MessageT>
    class SubscriptionTask : public ReactiveTask
    {
        using SubT = rclcpp::Subscription<MessageT>;
        // using SubT = rclcpp::SubscriptionBase;
    public:
        RCLCPP_SMART_PTR_DEFINITIONS(SubscriptionTask)

        // ----------------------------------------------------------------------------------------------------
        /**
         * @brief Construct a new Subscription Task object.
         *
         * The task is created in init state.
         *
         * @param[in] name The name of the task.
         * @param[in] cpu The cpu on which the task will execute.
         * @param[in] priority The priority of the task (higher priority first).
         * @param[in] period The periode of the task.
         * @param[in] jitter The jitter of the task.
         * @param[in] callback The function to execute at each periode.
         */
        SubscriptionTask(const std::string &name, int cpu, int priority, std::chrono::nanoseconds period, std::chrono::nanoseconds jitter, typename SubT::SharedPtr sub, bool start_on_spin = true)
            : sub_(sub),
              ReactiveTask(
                  name, cpu, priority, period, jitter,
                  [this]()
                  { return this->waitSet_.wait(0ms).kind() == rclcpp::WaitResultKind::Ready; },
                  [this]()
                  { this->execute_subscription(this->sub_); },
                  start_on_spin)
        {
            waitSet_.add_subscription(sub_);
        }
        virtual ~SubscriptionTask() {}

        // ----------------------------------------------------------------------------------------------------
    private:
        typename rclcpp::Subscription<MessageT>::SharedPtr sub_;
        // rclcpp::SubscriptionBase::SharedPtr sub_;
        // rclcpp::StaticWaitSet<1,0,0,0,0,0> waitSet_;
        rclcpp::WaitSet waitSet_;
    };

} // namespace corail_core

#endif
