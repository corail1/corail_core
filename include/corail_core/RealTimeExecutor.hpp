// Corail version 1.0, by Benoit Varillon and David Doose
// and Jean-Baptiste Chaudron and Charles Lesir-Cabaniols
// Copyright 2021-2022 ISAE-Supaero, Université de Toulouse, France

// This file is part of the Corail project.

// Corail is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// Corail is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.

// You should have received a copy of the GNU Lesser General Public License
// along with Corail.  If not, see <https://www.gnu.org/licenses/>.
// ---------------------------------------------------------------------------

#ifndef CORAIL_CORE__REALTIME_EXECUTOR_HPP
#define CORAIL_CORE__REALTIME_EXECUTOR_HPP

#include <string>
#include <functional>
#include <chrono>

#include "corail_core/RealTimeNode.hpp"

#include "rclcpp/rclcpp.hpp"

namespace corail_core
{

    /**
     * @brief Realtime implementation of rclcpp::Executor
     *
     * Use POSIX(pthread) to run each tasks (timers/subscriptions/services) in its own thread.
     * This executor can handle corail RealTimeNode as well as rclcpp Node.
     *
     */
    class RealTimeExecutor : public rclcpp::executors::SingleThreadedExecutor
    {
    public:
        RCLCPP_SMART_PTR_DEFINITIONS(RealTimeExecutor)

        /**
         * @brief Construct a new Real-Time Executor.
         *
         * @param name The name of the executor.
         * @param release Realease date of all the tasks.
         * @param timeout
         * @param options \see rclcpp::ExecutorOptions
         */
        RealTimeExecutor(const std::string &name,
                         std::chrono::nanoseconds release = std::chrono::seconds(1),
                         std::chrono::nanoseconds timeout = std::chrono::seconds(1),
                         const rclcpp::ExecutorOptions &options = rclcpp::ExecutorOptions())
            : SingleThreadedExecutor(options), name_(name), release_(release), timeout_(timeout) {}

        /**
         * @brief Add a corail node to the executor.
         *
         * @param node The node to add.
         */
        void add_rt_node(std::shared_ptr<RealTimeNode> node);
        void add_non_rt_node(std::shared_ptr<rclcpp::Node> node);

        void spin() override;

    private:
        friend class TaskBase;
        RCLCPP_DISABLE_COPY(RealTimeExecutor)
        std::string name_;
        std::chrono::nanoseconds release_;
        std::chrono::nanoseconds timeout_;
        std::vector<std::shared_ptr<RealTimeNode>> rt_nodes_;
        std::vector<std::shared_ptr<rclcpp::Node>> non_rt_nodes_;

        // needed to allow visibility of Executor::execute_<> in subscription and service task
        void static execute_subscription(rclcpp::SubscriptionBase::SharedPtr);
        void static execute_service(rclcpp::ServiceBase::SharedPtr);
    };
}

#endif