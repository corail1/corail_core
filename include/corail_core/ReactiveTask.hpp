// Corail version 1.0, by Benoit Varillon and David Doose
// and Jean-Baptiste Chaudron and Charles Lesir-Cabaniols
// Copyright 2021-2022 ISAE-Supaero, Université de Toulouse, France

// This file is part of the Corail project.

// Corail is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// Corail is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.

// You should have received a copy of the GNU Lesser General Public License
// along with Corail.  If not, see <https://www.gnu.org/licenses/>.
// ---------------------------------------------------------------------------

#ifndef CORAIL_CORE__REACTIVE_TASK_HPP
#define CORAIL_CORE__REACTIVE_TASK_HPP

#include <pthread.h>
#include <atomic>
#include <string>
#include <functional>
#include <chrono>

#include "utils/RtMutex.hpp"
#include "corail_core/TaskBase.hpp"
using namespace utils;

#include "rclcpp/rclcpp.hpp"

namespace corail_core
{

    //====================================================================================================
    /**
     * @brief Represent the possible states of the ReactiveTask state machine.
     *
     */
    enum class ReactiveTaskState
    {
        Init,
        Created,
        Configured,
        Started,
        Polling,
        Running,
        SleepingJitter,
        SleepingPeriod,
    };

    /**
     * @brief Convert a ReactiveTaskState object to string.
     *
     * @param state
     * @return std::string
     */
    std::string state_to_string(ReactiveTaskState state);
    std::ostream &operator<<(std::ostream &out, ReactiveTaskState state);

    //====================================================================================================
    /**
     * @brief Represent the transitions of the ReactiveTask state machine.
     *
     */
    enum class ReactiveTaskTransition
    {
        None,
        Terminate,
        Configure,
        Cleanup,
        Start,
        Stop_Sync,
        Stop_NoSync,
    };

    /**
     * @brief Convert a ReactiveTaskTransition object to string.
     *
     * @param transition
     * @return std::string
     */
    std::string transition_to_string(ReactiveTaskTransition transition);
    std::ostream &operator<<(std::ostream &out, ReactiveTaskTransition transition);

    //====================================================================================================
    /**
     * @brief Allow creation of reactive tasks.
     *
     * A state machine is used to controle the task excution. see \sa(ReactiveTaskState) and \sa(ReactiveTaskTrasition).
     *
     */
    class ReactiveTask : public TaskBase
    {
    public:
        RCLCPP_SMART_PTR_DEFINITIONS(ReactiveTask)

        // ----------------------------------------------------------------------------------------------------
        /**
         * @brief Construct a new Reactive Task object.
         *
         * The task is created in init state.
         *
         * @param[in] name The name of the task.
         * @param[in] cpu The cpu on which the task will execute.
         * @param[in] priority The priority of the task (higher priority first).
         * @param[in] period The periode of the task.
         * @param[in] jitter The jitter of the task.
         * @param[in] polling_callback The function to execute et each periode.
         * @param[in] execution_callback The function to execute if the polling callback return true.
         */
        ReactiveTask(const std::string &name,
                     int cpu,
                     int priority,
                     std::chrono::nanoseconds period,
                     std::chrono::nanoseconds jitter,
                     std::function<bool(void)> polling_callback,
                     std::function<void(void)> execution_callback,
                     bool start_on_spin = true)
            : name_(name), cpu_(cpu), priority_(priority), period_(period), jitter_(jitter),
              state_(ReactiveTaskState::Init), transition_(ReactiveTaskTransition::None), result_(false),
              next_release_({0, 0}),
              polling_callback_(polling_callback), execution_callback_(execution_callback),
              start_on_spin_(start_on_spin)
        {
            self_id_ = pthread_self();
        }
        virtual ~ReactiveTask() {}

        // ----------------------------------------------------------------------------------------------------

        /**
         * @brief Get the name
         *
         * @return std::string
         */
        inline std::string get_name()
        {
            mutex_.lock();
            std::string s = name_;
            mutex_.unlock();
            return s;
        }
        /**
         * @brief Get the cpu
         *
         * @return int
         */
        inline int get_cpu() const { return cpu_; }
        /**
         * @brief Get the priority
         *
         * @return int
         */
        inline int get_priority() const { return priority_; }
        /**
         * @brief Get the period
         *
         * @return std::chrono::nanoseconds
         */
        inline std::chrono::nanoseconds get_period() const { return period_; }

        /**
         * @brief Get the state
         *
         * @return PeriodicTaskState
         */
        inline ReactiveTaskState get_state() const { return state_; }

        // ----------------------------------------------------------------------------------------------------

        /**
         * @brief Trigger the create transition.
         *
         * Launch the creation of the thread in which the task will execute.
         * Only allowed in init state. -> created
         *
         * @return true If the transition succed
         * @return false Otherwise
         */
        bool create();
        /**
         * @brief Trigger the configure transition.
         *
         * Configure the thread priority and cpu.
         * Only allowed in created state. -> configured
         *
         * @return true If the transition succed
         * @return false Otherwise
         */
        bool configure();
        /**
         *
         * @brief Trigger the cleanup transition.
         *
         * Only allowed in configured state. -> created
         * @return true If the transition succed
         * @return false Otherwise
         */
        bool cleanup();
        /**
         * @brief Trigger the start transition.
         *
         * Lauch execution of the task at the asked date.
         * Only allowed in configured state. -> started
         *
         * @param[in] release The date of the first execution of the callback.
         * @return true If the transition succed
         * @return false Otherwise
         */
        bool start(std::chrono::nanoseconds release = std::chrono::nanoseconds(0));
        /**
         * @brief Trigger the stop transition.
         *
         * Stop the execution of the task.
         * Only allowed in started, running or sleeping states. -> configured
         *
         * @return true If the transition succed
         * @return false Otherwise
         */
        bool stop(bool synchronized = true);
        /**
         * @brief Trigger the terminate transition.
         *
         * Will destruct the thread of the task.
         * Only allowed in created state. -> init
         *
         * @return true If the transition succed
         * @return false Otherwises
         */
        bool terminate();

        inline bool start_on_spin() const { return this->start_on_spin_; }

        // ----------------------------------------------------------------------------------------------------
    private:
        static void *thread_start(void *data);

        void step();

        // ----------------------------------------------------------------------------------------------------
        std::string name_;
        pthread_t self_id_;
        // std::atomic<int> policy_;
        std::atomic<int> cpu_;
        std::atomic<int> priority_;
        std::atomic<std::chrono::nanoseconds> period_;
        std::atomic<std::chrono::nanoseconds> jitter_;
        //
        std::atomic<ReactiveTaskState> state_;
        std::atomic<ReactiveTaskTransition> transition_;
        std::atomic<bool> result_;
        timespec next_release_;
        //
        RtMutex mutex_;
        pthread_barrier_t barrier_;
        //
        std::function<bool(void)> polling_callback_;
        std::function<void(void)> execution_callback_;
        //
        bool start_on_spin_;
    };

} // namespace corail_core

#endif
