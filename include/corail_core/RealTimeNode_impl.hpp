// Corail version 1.0, by Benoit Varillon and David Doose
// and Jean-Baptiste Chaudron and Charles Lesir-Cabaniols
// Copyright 2021-2022 ISAE-Supaero, Université de Toulouse, France

// This file is part of the Corail project.

// Corail is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// Corail is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.

// You should have received a copy of the GNU Lesser General Public License
// along with Corail.  If not, see <https://www.gnu.org/licenses/>.
// ---------------------------------------------------------------------------

#ifndef CORAIL_CORE__REALTIME_NODE_IMPL_HPP
#define CORAIL_CORE__REALTIME_NODE_IMPL_HPP

#include "corail_core/SubscriptionTask.hpp"
#include "corail_core/RealTimeNode.hpp"
#include "corail_core/ServiceTask.hpp"

#include "rclcpp/create_subscription.hpp"
#include "rclcpp/node.hpp"

namespace corail_core
{
    template <
        typename MessageT,
        typename CallbackT,
        typename AllocatorT,
        typename CallbackMessageT,
        typename SubscriptionT,
        typename MessageMemoryStrategyT>
    typename SubscriptionTask<MessageT>::SharedPtr
    RealTimeNode::create_rt_subscription(
        const std::string &name,
        uint64_t cpu,
        uint64_t priority,
        std::chrono::microseconds polling_period,
        std::chrono::microseconds running_period,
        const std::string &topic_name,
        const rclcpp::QoS &qos,
        CallbackT &&callback,
        bool start_on_spin,
        const rclcpp::SubscriptionOptionsWithAllocator<AllocatorT> &options,
        typename MessageMemoryStrategyT::SharedPtr msg_mem_strat)
    {
        std::shared_ptr<SubscriptionT> sub = rclcpp::Node::create_subscription<MessageT>(topic_name, qos, callback, options, msg_mem_strat);
        auto rtSub = std::make_shared<SubscriptionTask<MessageT>>(name, cpu, priority, running_period, polling_period, sub, start_on_spin);
        subscription_tasks_.push_back(rtSub);
        return rtSub;
    }

    template <
        typename ServiceT,
        typename CallbackT>
    typename ServiceTask<ServiceT>::SharedPtr
    RealTimeNode::create_rt_service(
        const std::string name,
        uint64_t cpu,
        uint64_t priority,
        std::chrono::microseconds polling_period,
        std::chrono::microseconds running_period,
        const std::string &service_name,
        CallbackT &&callback,
        bool start_on_spin,
        const rmw_qos_profile_t &qos_profile,
        rclcpp::CallbackGroup::SharedPtr group)
    {
        std::shared_ptr<rclcpp::Service<ServiceT>> serv = rclcpp::Node::create_service<ServiceT>(service_name, callback, qos_profile, group);
        auto rtServ = std::make_shared<ServiceTask<ServiceT>>(name, cpu, priority, running_period, polling_period, serv, start_on_spin);
        service_tasks_.push_back(rtServ);
        return rtServ;
    }
} // namspece corail_core
#endif