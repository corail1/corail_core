// Corail version 1.0, by Benoit Varillon and David Doose
// and Jean-Baptiste Chaudron and Charles Lesir-Cabaniols
// Copyright 2021-2022 ISAE-Supaero, Université de Toulouse, France

// This file is part of the Corail project.

// Corail is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// Corail is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.

// You should have received a copy of the GNU Lesser General Public License
// along with Corail.  If not, see <https://www.gnu.org/licenses/>.
// ---------------------------------------------------------------------------

#ifndef UTILS__RT_TOOLS__HPP
#define UTILS__RT_TOOLS__HPP

#include <string>
#include <vector>
#include <chrono>

#include "pthread.h"

namespace utils
{
  /**
   * @brief The clock used by corail for all timer.
   *
   */
  constexpr auto CLOCK = CLOCK_MONOTONIC;

  /**
   * @brief Convert a pthread policy to string.
   *
   * @param[in] policy The policy to convert.
   * @return std::string, the policy as string.
   */
  std::string policy_to_str(const int &policy);

  /**
   * @brief Add a duration to a timespec.
   *
   * @param[in,out] clock The timespec to add the duration.
   * @param[in] ns The duration to add in nano seconds.
   */
  void add_ns(timespec &clock, uint64_t ns);

  /**
   * @brief Set a timespec to the specified date.
   *
   * @param[out] clock The timespec to set.
   * @param[in] ns The date to set in the timespec in nano seconds.
   */
  void set_ns(timespec &clock, uint64_t ns);

  /**
   * @brief Return the current date in nano seconds. Use CLOCK.
   *
   * @return uint64_t
   */
  uint64_t now_ns();

  /**
   * @brief Add two timespec.
   *
   * @param[in,out] t1 The first timespec to add, will get the result of the operation.
   * @param[in] t2 The second timespec to add, will remain unchange after the operation.
   */
  void timespec_add(timespec &t1, const timespec &t2);

  /**
   * @brief Convert a pthred timespec to a nb of nano seconds.
   *
   * @param[in] time The pthread timespec to convert.
   * @return int64_t The corresponding amout of nano seconds.
   */
  uint64_t timespec_to_nsec(const timespec &time);

  /**
   * @brief Fix the cpu of the current thread.
   *
   * @param[in] cpu
   * @return true
   * @return false
   */
  bool set_cpu(int cpu);

  /**
   * @brief Set the priority of the current thread.
   *
   * If priority > o the policy is set to SCHED_FIFO, otherwise it is set to SHED_OTHER
   *
   * @param[in] priority
   * @return true
   * @return false
   */
  bool set_priority(int priority);
}

#endif