// Corail version 1.0, by Benoit Varillon and David Doose
// and Jean-Baptiste Chaudron and Charles Lesir-Cabaniols
// Copyright 2021-2022 ISAE-Supaero, Université de Toulouse, France

// This file is part of the Corail project.

// Corail is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// Corail is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.

// You should have received a copy of the GNU Lesser General Public License
// along with Corail.  If not, see <https://www.gnu.org/licenses/>.
// ---------------------------------------------------------------------------

#ifndef UTILS_CPU_HPP
#define UTILS_CPU_HPP

#include <iostream>
#include <cstdint>
#include <sys/time.h>
#include <sys/resource.h>

namespace utils
{

    uint64_t total_usage(const struct rusage &usage);
    
    /**
     * @brief Block the cpu for an amont of time. Usefull for simulate a working task.
     * 
     * @param microsec 
     * @return uint64_t 
     */
    uint64_t burn_cpu(uint64_t microsec);

}

#endif
