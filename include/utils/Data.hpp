// Corail version 1.0, by David Doose
// Copyright 2023- ONERA

// This file is part of the Corail project.

// Corail is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// Corail is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.

// You should have received a copy of the GNU Lesser General Public License
// along with Corail.  If not, see <https://www.gnu.org/licenses/>.
// ---------------------------------------------------------------------------

#ifndef UTILS_RT_DATA
#define UTILS_RT_DATA

#include "RtMutex.hpp"

namespace utils
{
    /**
     * @brief Implements a realtime shared data.
     *
     */

    template <typename T>
    class RtData
    {
    public:
        /**
         * @brief Construct a new Rt Mutex object
         *
         */
        RtData(T data);
        RtData(RtData const &other) = delete;
        virtual ~RtData() noexcept;

        /**
         * @brief store data
         *
         */
        void store(T data);
        /**
         * @brief load data
         *
         */
        T load();

    private:
        RtMutex mutex_;
        T data_;
    };

}

#endif
