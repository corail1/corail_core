// Corail version 1.0, by Benoit Varillon and David Doose
// and Jean-Baptiste Chaudron and Charles Lesir-Cabaniols
// Copyright 2021-2022 ISAE-Supaero, Université de Toulouse, France

// This file is part of the Corail project.

// Corail is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// Corail is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.

// You should have received a copy of the GNU Lesser General Public License
// along with Corail.  If not, see <https://www.gnu.org/licenses/>.
// ---------------------------------------------------------------------------

#ifndef UTILS__SYNCHRO_THREAD_HPP
#define UTILS__SYNCHRO_THREAD_HPP

#include "utils/SynchroBarrier.hpp"

#include <pthread.h>

namespace utils
{

    /**
     * @brief Implementation of SynchroBarrier that work for threads
     *
     * This implementation rely on phtread_barrier and is only usable for synchronising threads in the same process
     *
     */
    class SynchroThread : public SynchroBarrier
    {

    public:
        SynchroThread(int n);
        virtual ~SynchroThread();
        void wait();

    private:
        pthread_barrier_t barrier_;
    };

} // namespace utils

#endif