# Corail Core
Corail version 1.0, by Benoit Varillon and David Doose
and Jean-Baptiste Chaudron and Charles Lesir-Cabaniols
Copyright 2021-2022 ISAE-Supaero, Université de Toulouse, France

Corail_core is the main package of the Corail project, it contain all the core functionalities of Corail.

Corail_core depends on rclcpp and corail_tracing.

For documentation and infromation about installation go to [corail1.gitlab.io](https://corail1.gitlab.io/)
